import { Component, OnInit } from '@angular/core';
import { Reserva } from 'src/app/models/reserva';
import { ReservaService } from 'src/app/service/reserva.service';

@Component({
  selector: 'app-list-reserva',
  templateUrl: './list-reserva.component.html',
  styleUrls: ['./list-reserva.component.css']
})
export class ListReservaComponent implements OnInit {

  reservas: Reserva[] = [];

  constructor(private reservaService: ReservaService) { }

  ngOnInit() {
    this.reservaService.lista().subscribe(
      data => {
        this.reservas = data;
      },
      err => {
        console.log(err);
      }
    );
  }

}
