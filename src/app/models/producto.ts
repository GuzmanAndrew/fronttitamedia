export class Producto {
    id?: number;
    name: string;
    autor: string;
    category: string;
    disponibility: number;
    price: number;

    constructor(name: string, autor: string, category: string, disponibility: number,
        price: number) {
        this.name = name;
        this.autor = autor;
        this.category = category;
        this.disponibility = disponibility;
        this.price = price;
    }
}


