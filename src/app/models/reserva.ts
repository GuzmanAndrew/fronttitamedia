export class Reserva {
   id?: number;
   name: string;
   price: number;
   fecha: string;
   disponibility: number;
   total: number;

   constructor(name: string, fecha: string, total: number, disponibility: number,
       price: number) {
       this.name = name;
       this.fecha = fecha;
       this.total = total;
       this.disponibility = disponibility;
       this.price = price;
   }
}
