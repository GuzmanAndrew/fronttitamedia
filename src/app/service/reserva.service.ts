import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Reserva } from '../models/reserva';

@Injectable({
  providedIn: 'root'
})
export class ReservaService {
  url = 'http://18.117.233.128:8080/api';

  constructor(private httpClient: HttpClient) { }

  public lista(): Observable<Reserva[]> {
    return this.httpClient.get<Reserva[]>(`${this.url}/list/reserva`);
  }

  public save(producto: Reserva): Observable<any> {
    return this.httpClient.post<any>(`${this.url}/save/reserva`, producto);
  }
}
